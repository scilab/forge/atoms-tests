// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Pierre MARECHAL <pierre.marechal@scilab.org>
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- ATOMS MANDATORY -->
// <-- CLI SHELL MODE -->

this_toolbox_name = "toolbox_2";
atomsLoad(this_toolbox_name);
this_toolbox_path = atomsGetLoadedPath(this_toolbox_name);
t2_common         = pathconvert(this_toolbox_path+"/tests/unit_tests/t2_common.txt",%F);

ref = [ "Toolbox 2 -> Function 1" ; "Toolbox 1 -> Function 1" ];
if or( t2_function1() <> ref ) then pause, end
if isempty(fileinfo(t2_common)) then pause, end
t2_common_mat = mgetl(t2_common);
if t2_function1()<>t2_common_mat(1:2) then pause, end
