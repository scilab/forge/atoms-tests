// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Pierre MARECHAL <pierre.marechal@scilab.org>
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- ATOMS MANDATORY -->
// <-- CLI SHELL MODE -->

this_toolbox_name = "toolbox_5";
atomsLoad(this_toolbox_name);
this_toolbox_path = atomsGetLoadedPath(this_toolbox_name);
t5_common         = pathconvert(this_toolbox_path+"/tests/unit_tests/t5_common.txt",%F);

ref = [ "Toolbox 5 -> Function 2" ; "Toolbox 4 -> Function 2" ; "Toolbox 2 -> Function 2" ; "Toolbox 1 -> Function 2" ];
if or( t5_function2() <> ref ) then pause, end
if isempty(fileinfo(t5_common)) then pause, end
t5_common_mat = mgetl(t5_common);
if t5_function2()<>t5_common_mat(5:8) then pause, end
