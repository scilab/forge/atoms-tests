//exec all cleaner.sce and delete "binaries" files ( .jar, .bin, ...)

main_dir = get_absolute_file_path("clean_all.sce");

exec(main_dir + "tbx.sce");

function rmdoc(path)
    dirs = ls(path);
    dirs_to_delete = path + "/" + dirs + "/scilab_" + dirs + "_help";
    for i = 1 : size(dirs_to_delete, "*")
        rmdir(dirs_to_delete(i), "s");
    end

    //master_help file
    files_to_delete = path +"/" + dirs + "/master_help.xml";
    for i = 1 : size(files_to_delete, "*")
        deletefile(files_to_delete(i));
    end
endfunction

for kn=1:size(tbx_names, "*")
    for kv=1:size(tbx_versions(kn), "*")
        macrospath = main_dir + tbx_names(kn) + "/" + tbx_versions(kn)(kv) + "/macros";
        deletefile(macrospath + "/lib");
        deletefile(macrospath + "/names");
        allbins = ls(macrospath + "/*.bin");
        for kb=1:size(allbins, "*")
            deletefile(allbins(kb));
        end
        
        jarpath = main_dir + tbx_names(kn) + "/" + tbx_versions(kn)(kv) + "/jar";
        rmdir(jarpath, "s");
        
        loaderpath = main_dir + tbx_names(kn) + "/" + tbx_versions(kn)(kv) + "/loader.sce";
        deletefile(loaderpath);
        
        unloaderpath = main_dir + tbx_names(kn) + "/" + tbx_versions(kn)(kv) + "/unloader.sce";
        deletefile(unloaderpath);
        
        docpath =  main_dir + tbx_names(kn) + "/" + tbx_versions(kn)(kv) + "/help";
        rmdoc(docpath);
    end
end

exit