// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Pierre MARECHAL <pierre.marechal@scilab.org>
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- ATOMS MANDATORY -->
// <-- CLI SHELL MODE -->

this_toolbox_name = "toolbox_1";
atomsLoad(this_toolbox_name);
this_toolbox_path = atomsGetLoadedPath(this_toolbox_name);
t1_common         = pathconvert(this_toolbox_path+"/tests/unit_tests/t1_common.txt",%F);

if t1_function3() <> "Toolbox 1 -> Function 3" then pause, end
if isempty(fileinfo(t1_common)) then pause, end
t1_common_mat = mgetl(t1_common);
if t1_function3()<>t1_common_mat(3) then pause, end
