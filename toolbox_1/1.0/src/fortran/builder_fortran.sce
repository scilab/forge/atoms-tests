// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

tbx_build_src(['t1_function6'], ['t1_function6.f'], 'f', ..
              get_absolute_file_path('builder_fortran.sce'));

clear tbx_build_src;
