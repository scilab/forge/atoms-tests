// ====================================================================
// Allan CORNET
// Simon LIPP
// INRIA 2008
// This file is released into the public domain
// ====================================================================

tbx_build_src(['t1_function4'], ['t1_function4.c'], 'c', ..
              get_absolute_file_path('builder_c.sce'));

clear tbx_build_src;
