// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

tbx_build_gateway('toolbox_1_c', ['t1_function4','sci_t1_function4'], ['sci_t1_function4.c'], ..
                  get_absolute_file_path('builder_gateway_c.sce'), ..
                  ['../../src/c/libt1_function4'], ..
                  [], ..
                  "-D__USE_DEPRECATED_STACK_FUNCTIONS__");

clear tbx_build_gateway;
