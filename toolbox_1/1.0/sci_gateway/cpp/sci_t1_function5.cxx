/* ==================================================================== */
/* Allan CORNET */
/* DIGITEO 2009 */
/* Template toolbox_skeleton */
/* This file is released into the public domain */
/* ==================================================================== */

#include <string>

extern "C" 
{
	#include <stdlib.h>
	#include "api_scilab.h"
	#include "Scierror.h"

  int sci_t1_function5(char *fname, void* pvApiCtx) 
  {
	  int m_out = 1, n_out = 1;
	  char **OutputString = NULL;
	  char *result = "Toolbox 1 -> Function 5\n";

	  /* Check the number of input argument */
	  CheckRhs(0,0); 

	  /* Check the number of output argument */
	  CheckLhs(1,1);

	  OutputString = (char**)malloc(sizeof(char*)*1);

	  if (OutputString)
	  {
		  OutputString[0] = result;
		  createMatrixOfString(pvApiCtx, Rhs + 1, m_out, n_out, OutputString);
		  LhsVar(1) = Rhs + 1; 
		  free(OutputString);
		  OutputString = NULL;
	  }
	  else
	  {
		  Scierror(999,"%s: memory allocation error.\n",fname);
	  }

	  return 0;
  }

} /* extern "C" */
