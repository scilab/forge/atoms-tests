
#include "stack-c.h" 
#include "Scierror.h"
#include "MALLOC.h"

extern int F2C(t1_function6)(double *a,double *b,double *c);

int sci_fsum(char *fname, void* pvApiCtx)
{ 
  int a = 2;
  int b = 4;
  
  int m_out = 0, n_out = 0;
  int *piAddressOut = NULL;
  double dOut = 0.0;

  CheckRhs(0,0) ;
  CheckLhs(1,1) ;
  
  /* call fortran subroutine fsum */
  F2C(t1_function6)(&a,&b,&dOut);
  
  sciprint("Toolbox 1 -> Function %d\n",&dOut);
  
  return 0;
}
